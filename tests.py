from unittest import TestCase
from unittest.mock import patch

from regexp import function_date, function_sum, function_del


class RegexpTestCase(TestCase):

    @patch('regexp.open_file', return_value='test')
    @patch('regexp.re.findall', return_value='test')
    def test_function_date(self, mock_re_findall, mock_open_file):
        expected_value = 'test'
        value = function_date()
        mock_open_file.assert_called_once_with("text_files/01_file.txt")
        mock_re_findall.assert_called_once_with(r"\d{2}-\d{2}-\d{4}", mock_open_file())
        self.assertEqual(expected_value, value)

    @patch('regexp.open_file', return_value='test')
    @patch('regexp.re.findall', return_value=[50, 50])
    def test_function_sum(self, mock_re_findall, mock_open_file):
        expected_sum = 100
        sum = function_sum()
        mock_open_file.assert_called_once_with("text_files/02_file.txt")
        mock_re_findall.assert_called_once_with(r"-?\d+(?:\.\d+)?", mock_open_file())
        self.assertEqual(expected_sum, sum)

    @patch('regexp.open_file', return_value='test')
    @patch('regexp.overwrite_file')
    @patch('regexp.re.sub', return_value='test')
    def test_function_del(self, mock_re_sub, mock_overwrite_file, mock_open_file):
        expected_value = 'test'
        value = function_del()
        mock_open_file.assert_called_once_with("text_files/03_file.txt")
        mock_overwrite_file.assert_called_once()
        mock_re_sub.assert_called_once_with(r"-\d+(?:\.\d+)?", " ", mock_open_file())
        self.assertEqual(expected_value, value)
