import re


def open_file(file):
    with open(file, "r") as f:
        content = f.read()
        return content


def overwrite_file(file, content):
    with open(file, "w") as f3:
        f3.write(content)


def function_date():
    regexp1 = re.findall(r"\d{2}-\d{2}-\d{4}", open_file("text_files/01_file.txt"))
    print(regexp1)
    return regexp1


def function_sum():
    regexp2 = re.findall(r"-?\d+(?:\.\d+)?", open_file("text_files/02_file.txt"))

    sum = 0
    for num in regexp2:
        sum += float(num)

    print(sum)
    return sum


def function_del():
    content = open_file("text_files/03_file.txt")
    regexp3 = re.sub(r"-\d+(?:\.\d+)?", " ", content)

    print(regexp3)
    overwrite_file("text_files/03_file.txt", regexp3)
    return regexp3


if __name__ == "__main__":
    function_date()
    function_sum()
    function_del()
